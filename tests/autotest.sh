#!/bin/sh

# Set to non-empty for interactive test (show both PDFs)
interactive=
T=

DIR=$(cd "$(dirname "$0")" && pwd)
PATH="$DIR"/..:"$PATH"

die () {
    printf "%s\n" "$@"
    exit 1
}

cd "$DIR" || die "Could not cd to $DIR"

mkdir -p actual/err expected/err actual/txt expected/txt

test_file () {
    actual="$DIR/actual/$T$1"
    actualtxt="$DIR/actual/txt/$T$1"
    actualerr="$DIR/actual/err/$T$1"
    expected="$DIR/expected/$T$1"
    expectedtxt="$DIR/expected/txt/$T$1"
    expectederr="$DIR/expected/err/$T$1"
    echo latexpand "$@"
    latexpand "$@" >"$actual" 2>"$actualerr" || die "latexpand failed for $*"

    pdflatex -interaction=nonstopmode "$1" > /dev/null || die "Compilation of non-expanded file failed for $*"
    (cd "$DIR/actual" && TEXINPUTS=..:"$TEXINPUTS" pdflatex -interaction=nonstopmode "$actual" >/dev/null) || die "Compilation of expanded file failed for $*"
    pdftotext "${1%.tex}.pdf" "$expectedtxt"
    pdftotext "${actual%.tex}.pdf" "$actualtxt"

    diff -u "$expectedtxt" "$actualtxt" || die "Difference found for $*"

    if test -n "$interactive"
    then
	xpdf "${actual%.tex}.pdf"
	xpdf "${1%.tex}.pdf"
    fi

    if test -e "$expected"
    then
	# Get rid of absolute path.
	sed -i "s@$DIR@.@g" "$actual"
	diff -u "$expected" "$actual" || die "Actual expanded did not match expected"
	rm -f "$actual"
    fi
    if test -e "$expectederr"
    then
	# Get rid of absolute path.
	sed -i "s@$DIR@.@g" "$actualerr"
	diff -u "$expectederr" "$actualerr" || die "Actual stderr did not match expected"
	rm -f "$actualerr"
    fi

    T=
}

T=comments- test_file strip-comments.tex

test_file includer-comment.tex
test_file includer-with-end.tex
test_file includer.tex
T=empty-comments- test_file includer.tex --empty-comments
T=explain- test_file includer.tex --explain
T=explain-comments- test_file includer.tex --explain --empty-comments
test_file includegraphic.tex --show-graphics

(
    cd df-conflict/
    test_file a.tex
)

T=usepackage- test_file package-user.tex --expand-usepackage
T=usepackagecomments- test_file package-user.tex --expand-usepackage --keep-comments
test_file package-user.tex

T=keep-comments- test_file text-after-end.tex --keep-comments
test_file text-after-end.tex

test_file makeatletter.tex --makeatletter
# Known failure
! ( test_file makeatletter.tex )

echo 'testing @-commands warning...'
if ! latexpand makeatletter.tex 2>&1 | grep -q 'containing @'
then
    die 'latexpand did not warn on @-command'
fi
if latexpand --makeatletter makeatletter.tex 2>&1 | grep -q 'containing @'
then
    die 'latexpand did warn on @-command even with --makeatletter'
fi
echo 'testing @-commands warning... done'

echo 'Testing subimport support...'
latexpand hello_subimport.tex > actual/hello_subimport-o.tex
diff -u actual/hello_subimport-o.tex expected/hello_subimport.tex ||
    die "subimport failed"

echo 'Testing import support...'
sed "s@PWD@$PWD@" hello_import.tex.in > hello_import.tex
sed "s@PWD@$PWD@" expected/hello_import.tex.in > expected/hello_import.tex
latexpand hello_import.tex > actual/hello_import-o.tex
diff -u actual/hello_import-o.tex expected/hello_import.tex ||
    die "import failed"

echo 'Testing CLI options...'
latexpand includer-comment.tex -o actual/dash-dash-o.tex ||
    die "Option -o unsupported"
diff -u actual/dash-dash-o.tex expected/includer-comment.tex ||
    die "Incorrect output on -o"

latexpand includer-comment.tex --output actual/dash-dash-output.tex ||
    die "Option --output unsupported"
diff -u actual/dash-dash-output.tex expected/includer-comment.tex ||
    die "Incorrect output on --output"

latexpand includer-comment.tex -o - > actual/dash-o-dash.tex ||
    die "Option -o - unsupported"
diff -u actual/dash-o-dash.tex expected/includer-comment.tex ||
    die "Incorrect output on -o"
echo 'Testing CLI options... done'
